import configparser
import os
import stat
import sys
import subprocess
import time

import yaml
from bitbucket_pipes_toolkit import get_logger

from pipe import main as cfn_pipe


MSG_AWS_SAM_DEPLOY_FAILED = 'Failed SAM deploy'
MSG_AWS_SAM_BUILD_FAILED = 'Failed SAM build'
MSG_AWS_SAM_PACKAGE_FAILED = 'Failed SAM package'
DEFAULT_SAM_BUILD_DIR_FILENAME = ".aws-sam"
DEFAULT_SAM_STACK_NAME_FILENAME = "sam-bitbucket-pipes"
DEFAULT_SAM_TEMPLATE_FILENAME = 'template.yaml'
DEFAULT_CFN_TEMPLATE_FILENAME = 'packaged.yml'


schema_commands = ["all", "package-only", "deploy-only", "build-only", "deploy-artifacts", "build-and-deploy"]

common_schema = {
    "AWS_ACCESS_KEY_ID": {
        "type": "string",
        "required": True
    },
    "AWS_SECRET_ACCESS_KEY": {
        "type": "string",
        "required": True
    },
    "AWS_DEFAULT_REGION": {
        "type": "string",
        "required": True
    },
    "COMMAND": {
        "type": "string",
        "allowed": schema_commands,
        "default": schema_commands[0]
    },
}

cloudformation_schema = cfn_pipe.schema
package_schema = {
    "S3_BUCKET": {
        "type": "string",
        "required": True
    },
    "SAM_TEMPLATE": {
        "type": "string",
        "default": DEFAULT_SAM_TEMPLATE_FILENAME
    },
    "CFN_TEMPLATE": {
        "type": "string",
        "default": DEFAULT_CFN_TEMPLATE_FILENAME
    },
    "SAM_BUILD_DIR": {
        "type": "string",
        "default": DEFAULT_SAM_BUILD_DIR_FILENAME
    },
    "STACK_NAME": {
        "type": "string",
        "default": DEFAULT_SAM_STACK_NAME_FILENAME
    },
    'SAM_CONFIG': {
        "type": "string",
        "required": False
    },
    'DEBUG': {
        "type": "boolean",
        "default": False,
        "required": False
    },
}
package_schema.update(common_schema)

logger = get_logger()


class SamDeployPipe(cfn_pipe.CloudformationDeployPipe):
    OIDC_AUTH = 'OIDC_AUTH'
    DEFAULT_AUTH = 'DEFAULT_AUTH'

    def __init__(self, pipe_metadata=None, schema=None, env=None, client=None, check_for_newer_version=False):
        self.auth_method = self.discover_auth_method()
        super().__init__(pipe_metadata=pipe_metadata, schema=schema, env=env,
                         check_for_newer_version=check_for_newer_version)

    def discover_auth_method(self):
        """Discover user intentions: authenticate to AWS through OIDC or default aws access keys"""
        oidc_role = os.getenv('AWS_OIDC_ROLE_ARN')
        web_identity_token = os.getenv('BITBUCKET_STEP_OIDC_TOKEN')
        if oidc_role:
            if web_identity_token:
                logger.info('Authenticating with a OpenID Connect (OIDC) Web Identity Provider.')
                common_schema['BITBUCKET_STEP_OIDC_TOKEN'] = {'required': True}
                common_schema['AWS_OIDC_ROLE_ARN'] = {'required': True}

                common_schema['AWS_ACCESS_KEY_ID']['required'] = False
                common_schema['AWS_SECRET_ACCESS_KEY']['required'] = False

                os.environ.pop('AWS_ACCESS_KEY_ID', None)
                os.environ.pop('AWS_SECRET_ACCESS_KEY', None)

                cloudformation_schema.update(common_schema)
                package_schema.update(common_schema)
                return self.OIDC_AUTH

            logger.warning('Parameter `oidc: true` in the step configuration is required for OIDC authentication')
            logger.info('Using default authentication with AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY.')
            return self.DEFAULT_AUTH

        logger.info('Using default authentication with AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY.')
        return self.DEFAULT_AUTH

    def auth(self):
        """Authenticate via chosen method"""
        if self.auth_method == self.OIDC_AUTH:
            random_number = str(time.time_ns())
            aws_config_directory = os.path.join(os.environ["HOME"], '.aws')
            oidc_token_directory = os.path.join(aws_config_directory, '.aws-oidc')

            os.makedirs(aws_config_directory, exist_ok=True)
            os.makedirs(oidc_token_directory, exist_ok=True)

            web_identity_token_path = os.path.join(oidc_token_directory, f'oidc_token_{random_number}')
            with open(web_identity_token_path, 'w') as f:
                f.write(self.get_variable('BITBUCKET_STEP_OIDC_TOKEN'))

            os.chmod(web_identity_token_path, mode=stat.S_IRUSR)
            logger.debug('Web identity token file is created')

            aws_configfile_path = os.path.join(aws_config_directory, 'config')
            with open(aws_configfile_path, 'w') as configfile:
                config = configparser.ConfigParser()
                config['default'] = {
                    'role_arn': self.get_variable('AWS_OIDC_ROLE_ARN'),
                    'web_identity_token_file': web_identity_token_path
                }
                config.write(configfile)
            logger.debug('Configured settings for authentication with assume web identity role')

    def build(self):
        """
        Builds a serverless application and prepares it for subsequent steps in your workflow.
        The sam build command processes your AWS SAM template file, application code, and any applicable language-specific files and dependencies.
        https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/sam-cli-command-reference-sam-build.html
        :return:
        """
        logger.info(f'Start build...')

        sam_template_file = self.get_variable('SAM_TEMPLATE')
        sam_build_dir = os.path.join(os.getenv('BITBUCKET_PIPE_STORAGE_DIR'),self.get_variable('SAM_BUILD_DIR'))
        debug = self.get_variable('DEBUG')

        args = [
            "sam", "build",
            "--template-file", sam_template_file,
            "--build-dir" , sam_build_dir
        ]
        
        sam_config = self.get_variable('SAM_CONFIG')
        if sam_config:
            if not os.path.exists(sam_config):
                self.fail('Deploy has failed. Passed SAM_CONFIG path does not exist')
            args.extend(['--config-file', sam_config])

        if debug == True:
            args.append('--debug')


        build = subprocess.run(args,
                                check=False,
                                text=True,
                                encoding="utf-8",
                                stdout=subprocess.DEVNULL,
                                stderr=sys.stderr)
                                
        if build.returncode != 0:
            self.fail(f'{MSG_AWS_SAM_BUILD_FAILED}')
        logger.info(f'Build Succeeded')
        
            
    def deploy_artifacts(self,template=None):
        """
        Deploying AWS Lambda functions through AWS CloudFormation 
        requires an Amazon Simple Storage Service (Amazon S3) bucket for the Lambda deployment package
        https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/sam-cli-command-reference-sam-deploy.html
        :return:
        """
        logger.info(f'Start deploy...')

        s3_bucket = self.get_variable('S3_BUCKET')
        sam_template_file = self.get_variable('SAM_TEMPLATE')
        sam_stack_name = self.get_variable('STACK_NAME')
        debug = self.get_variable('DEBUG')
        
        if template is not None:
            args = [
                "sam", "deploy", 
                "--template-file", sam_template_file, 
                "--stack-name", sam_stack_name, 
                "--capabilities", "CAPABILITY_IAM", 
                "--s3-bucket", s3_bucket,
                "--no-confirm-changeset"
            ]
        else:
            args = [
                "sam", "deploy", 
                "--template-file", template,
                "--stack-name", sam_stack_name, 
                "--capabilities", "CAPABILITY_IAM", 
                "--s3-bucket", s3_bucket,
                "--no-confirm-changeset"
            ]
        
        sam_config = self.get_variable('SAM_CONFIG')
        if sam_config:
            if not os.path.exists(sam_config):
                self.fail('Deploy has failed. Passed SAM_CONFIG path does not exist')
            args.extend(['--config-file', sam_config])

        if debug == True:
            args.append('--debug') 

        result = subprocess.run(args,
                            check=False,
                            text=True,
                            encoding="utf-8",
                            stdout=subprocess.DEVNULL,
                            stderr=sys.stderr)

        if result.returncode != 0:
            self.fail(f'{MSG_AWS_SAM_DEPLOY_FAILED}')
        logger.info(f'Deploy Succeeded')
    
    def package(self):
        """
        Packages the local artifacts (local paths) that your AWS SAM template references.
        The command uploads local artifacts, such as source code for AWS Lambda functions, to an S3 bucket.
        https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/sam-cli-command-reference-sam-package.html
        :return:
        """
        logger.info(f'Start package...')
        s3_bucket = self.get_variable('S3_BUCKET')
        sam_template_file = self.get_variable('SAM_TEMPLATE')

        cloudformation_template_file = os.path.join(os.getenv('BITBUCKET_PIPE_STORAGE_DIR'),
                                                    self.get_variable('CFN_TEMPLATE'))
        debug = self.get_variable('DEBUG')

        args = [
            "sam", "package",
            "--template-file", sam_template_file,
            "--s3-bucket", s3_bucket,
            "--output-template-file", cloudformation_template_file
        ]
        sam_config = self.get_variable('SAM_CONFIG')
        if sam_config:
            if not os.path.exists(sam_config):
                self.fail('Packaging has failed. Passed SAM_CONFIG path does not exist')
            args.extend(['--config-file', sam_config])

        if debug:
            args.append('--debug')

        result = subprocess.run(args,
                                check=False,
                                text=True,
                                encoding="utf-8",
                                stdout=sys.stdout,
                                stderr=sys.stderr)
        if result.returncode != 0:
            self.fail(f'{MSG_AWS_SAM_PACKAGE_FAILED}')

        # set TEMPLATE to packaged_cloudformation template ready for deploy
        self.variables['TEMPLATE'] = cloudformation_template_file
        self.env['TEMPLATE'] = cloudformation_template_file
        logger.info(f'Packaged application uploaded to S3 bucket {s3_bucket} and generated CloudFormation template written to {cloudformation_template_file}.')

    def build_and_deploy(self):
        logger.info(f'Start build and deploy...')
        self.build()
        sam_build_dir = os.path.join(os.getenv('BITBUCKET_PIPE_STORAGE_DIR'), self.get_variable('SAM_BUILD_DIR'))
        self.deploy_artifacts(template=sam_build_dir+'/build/template.yaml')

    def run(self):
        self.enable_debug_log_level()
        self.auth()
        command = self.get_variable('COMMAND')

        if command == "package-only" or command == "all":
            # build artifacts packaged_template and upload zipped app to AWS S3
            self.schema = package_schema
            self.variables = self.validate()
            self.package()

        if command == "build-only" or command == "all":
            self.schema = package_schema
            self.variables = self.validate()
            self.build()
            
        if command == "deploy-artifacts" or command == "all":
            self.schema = package_schema
            self.variables = self.validate()
            self.deploy_artifacts()

        if command == "build-and-deploy" or command == "all":
            self.schema = package_schema
            self.variables = self.validate()
            self.build_and_deploy()

        if command == "deploy-only" or command == "all":
            # deploy App with aws-cloudformation-deploy using packaged.yml linked to S3 with App code
            self.schema = cloudformation_schema
            variables = self.validate()
            self.variables.update(variables)

            logger.info(f'Start deploy...')
            super().run()


if __name__ == '__main__':
    metadata = yaml.safe_load(open('/usr/bin/pipe.yml', 'r'))
    pipe = SamDeployPipe(schema=common_schema, pipe_metadata=metadata, check_for_newer_version=True)
    pipe.run()
